FROM ubuntu:20.04
RUN apt-get update && apt-get install -y lftp ca-certificates wget
RUN wget https://fastdl.mongodb.org/tools/db/mongodb-database-tools-ubuntu2004-x86_64-100.5.2.deb && dpkg -i mongodb-database-tools-ubuntu2004-x86_64-100.5.2.deb && rm mongodb-database-tools-ubuntu2004-x86_64-100.5.2.deb
RUN wget https://downloads.mongodb.com/compass/mongodb-mongosh_1.5.0_amd64.deb && dpkg -i mongodb-mongosh_1.5.0_amd64.deb && rm mongodb-mongosh_1.5.0_amd64.deb
